@extends('layouts.app')

@section('content')
    <div class="panel-body">
        @include('common.errors')
        <form action = "{{ url('task') }}" method="POST" class="form-horizontal">
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="task" class="col-sm-3 control-label">Task</label>

                <div class="form-group">
                    <input type="text" name="name" placeholder="Enter a task" id="task-name" class="form-control">
                    </div>
            </div>
            <div class="form-group">
                <button type="submit">Add Task</button>
            </div>
            </form>
        </div>

        @if(count($tasks)>0)
            <div class="panel panel-default">
                <div class="panel-heading">
                    Current Tasks
                </div>

                <div class="panel-body">
                    <table class="table table-striped task-table">
                        <thead>
                            <th>Task</th>
                            <th>Delete</th>
                        </thead>

                        <tbody>
                            @foreach($tasks as $task)
                                <tr>
                                    <td class="table-text">
                                        <div> {{ $task->name }} </div>
                                    </td>

                                    <td>
                                        <form action="{{ url('task/'.$task->id) }}" method="POST">
                                            {!! csrf_field() !!}
                                            {!! method_field('DELETE') !!}
                                            <button>Delete Task</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endif
    @endsection