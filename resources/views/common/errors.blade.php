@if(count($errors)>0)
    <div class="alert a;ert-danger">
        <strong>Error</strong>

        <ul>
            $foreach ($errors->all() as $error)
                <li> {{ $error }} </li>

        </ul>
    </div>
@endif